﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        public static List<SocketPipe> socketPipes = new List<SocketPipe>();
        public class SocketPipe : IDuplexPipe
        {
            NetworkStream _stream;
            Pipe _readPipe, _writePipe;
            bool active;
            public SocketPipe(NetworkStream stream)
            {
                _stream = stream;
                _readPipe = new Pipe();
                _writePipe = new Pipe();
                active = true;
            }
            public PipeReader Input => _readPipe.Reader;
            public PipeWriter Output => _writePipe.Writer;

            public async ValueTask handle()
            {

                _ = processData();
                _ = writeToStream();
                _ = readFromStream();
            }
            private async ValueTask readFromStream()
            {
                Console.WriteLine("readFromStream entered");
                while (active)
                {
                    Exception error = null;
                    try
                    {
                        // note we'll usually get *much* more than we ask for
                        var buffer = _readPipe.Writer.GetMemory(1);
                        int bytes = await _stream.ReadAsync(buffer);
                        Console.WriteLine($"Got {bytes} from stream");
                        _readPipe.Writer.Advance(bytes);
                        if (bytes == 0) continue; // source EOF

                        var flush = await _readPipe.Writer.FlushAsync();

                    }
                    catch (System.IO.IOException)
                    {
                        active = false;
                    }
                    catch (Exception ex) { error = ex; Console.WriteLine(ex.ToString()); }
                    finally {  }
                }
                Console.WriteLine("read from stream going night night");
            }

            private async ValueTask writeToStream()
            {
                Console.WriteLine("writeToStream entered");
                while (active)
                {
                    try
                    {
                        var read = await _writePipe.Reader.ReadAsync();
                        var buffer = read.Buffer;
                        Console.WriteLine($"Found {buffer.Length} ready to send");
                        if (read.IsCanceled) continue;
                        if (buffer.IsEmpty && read.IsCompleted) continue;

                        // write everything we got to the stream
                        foreach (var segment in buffer)
                        {
                            await _stream.WriteAsync(segment);
                        }
                        _writePipe.Reader.AdvanceTo(buffer.End);
                        await _stream.FlushAsync();
                    } catch (System.IO.IOException)
                    {
                        active = false;
                    }
                    

                }
            }

            private async ValueTask processData()
            {
                Console.WriteLine("processData entered");
                while (active)
                {
                    try
                    {
                        var read = await Input.ReadAsync();
                        var buffer = read.Buffer;
                        Console.WriteLine($"Found {buffer.Length} in the process pool");
                        if (read.IsCanceled)
                            continue;
                        if (buffer.IsEmpty && read.IsCompleted) 
                            continue;
                        var buffer2 = Output.GetMemory((int)buffer.Length);
                        int byteTotal = 0;
                        foreach (var segment in buffer)
                        {
                            int bytes = segment.Length;
                            if (bytes == 0) break; // source EOF
                            segment.CopyTo(buffer2.Slice(byteTotal, bytes));
                            Output.Advance(bytes);
                            var flush = await Output.FlushAsync();
                        }
                        Input.AdvanceTo(buffer.End);
                    } catch (System.IO.IOException)
                    {
                        active = false;
                    }
                    catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                
                }
            }
        }

        
        static async Task Main(string[] args)
        {
            var listenSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            listenSocket.Bind(new IPEndPoint(IPAddress.Loopback, 8087));

            Console.WriteLine("Listening on port 8087");

            listenSocket.Listen(120);

            while (true)
            {
                Console.WriteLine("Listening...");
                socketPipes.Add(new SocketPipe(new NetworkStream(await listenSocket.AcceptAsync())));
                _ = socketPipes[socketPipes.Count - 1].handle();
            }
        }
    }
}
